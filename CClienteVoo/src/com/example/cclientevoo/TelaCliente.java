package com.example.cclientevoo;

import java.security.PublicKey;

import com.example.cclientevoo.R.layout;
import com.example.cclientevoo.R.string;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

public class TelaCliente extends Activity {
	//declara�ao do adapter para o Spinner
	ArrayAdapter<String> opcoes;
	//declara��o do array para o Spinner
	String[] arrayAeroportos = {"PMW - Palmas-TO", "BSB - Brasilia-DF", "GYN - Goiania-GO", "CGH - S�o Paulo-SP" };
	//Spinner da tela
	Spinner spinner;
	//Button btconsultar;
	//Button btvoltar;
	
	String entradaSocket;
		

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_cliente);
        
        Button btconsultar = (Button)findViewById(R.id.btconsultar);
        btconsultar.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				telaempresas();
								
			}
		});
        
       
        
        opcoes = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,arrayAeroportos);
        
        //pega a instancia do objeto no XML
        spinner = (Spinner) findViewById(R.id.spinner1);
        
        //seta as op�oes para Spinner
        spinner.setAdapter(opcoes);
       
    }
    
    public void telaempresas()
    {
    	
    	
    	entradaSocket = (String)spinner.getSelectedItem();
    	setContentView(R.layout.telaempresas);
    	
    	Button btvoltar = (Button)findViewById(R.id.btvoltar);
    	Button btconsultavoo = (Button)findViewById(R.id.btconsultavoo);
        btvoltar.setOnClickListener(new View.OnClickListener() {
 			
 			public void onClick(View v) {
 				Chamatelacliente();
 	 				
 			}
 		});
        btconsultavoo.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				Telaconsulta();
				
			}
		});
       	
    }
    
//a�oes onclick no consultar voo para tela consulta
    public void Telaconsulta() 
    {
    	RadioGroup radioGroup = (RadioGroup)findViewById(R.id.radioEmpresa);
    	RadioButton radioSelecionado;
    	
    	
    	// get selected radio button from radioGroup
		int selectedId = radioGroup.getCheckedRadioButtonId();
    	 
		// find the radiobutton by returned id
		radioSelecionado = (RadioButton) findViewById(selectedId);
		entradaSocket +="|"+radioSelecionado.getText();
    	
    	setContentView(R.layout.ttelaconsulta);
    	
    	TextView tvResultados = (TextView) findViewById(R.id.tvResultado);
    	
    	String sResultado = "Aeroporto: "+entradaSocket+" \n";
    	sResultado +="Empresa: "+radioSelecionado.getText()+"\n";
    	sResultado +="N� Voo: 1111\n";
    	sResultado += "Tipo: I/O\n";
    	sResultado += "--------------------------------";
    	
    	tvResultados.setText(sResultado);
     
    	Button btvoltar = (Button)findViewById(R.id.btvoltar);
    	
        btvoltar.setOnClickListener(new View.OnClickListener() {
 			
 			public void onClick(View v) {
 				telaempresas();
 			
 				
 			}
 		});
		
	}
    
    
    
    
    
    
    
    
    public void Chamatelacliente() 
    {
    	setContentView(R.layout.activity_tela_cliente);
    	
    	 //pega a instancia do objeto no XML
          spinner = (Spinner) findViewById(R.id.spinner1);
    	 spinner.setAdapter(opcoes);
    	   
    	 Button btconsultar = (Button)findViewById(R.id.btconsultar);
           btconsultar.setOnClickListener(new View.OnClickListener() {
   			
   			public void onClick(View v) {
   				telaempresas();
   								
   			}
   		});
          
    	 	
	}
    
    
    
    
    
    
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_tela_cliente, menu);
        return true;
 
    }
}
